//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 1/16/2015 AppContext.h **//


#ifndef APPCONTEXT_H
#define APPCONTEXT_H

#include <memory>
#include <vector>

#include <boost/asio.hpp>

#include "Logger.h"
#include "Timer.h"
#include "PMutex.h"
#include "PPThread.h"

/**
 * @brief The AppContext class
 */

class AppContext {

public:

  /**
   * @brief Init
   * @param argc
   * @param argv
   * @return
   */
  bool Init(int& argc, char** argv) ;

  boost::asio::io_service& IOService() ;

  bool GetConfigData(const std::string& section, const std::string& group, const std::string& key, bool& value) ;
  bool GetConfigData(const std::string& section, const std::string& group, const std::string& key, int& value) ;
  bool GetConfigData(const std::string& section, const std::string& group, const std::string& key, long& value) ;
  bool GetConfigData(const std::string& section, const std::string& group, const std::string& key, long long& value) ;
  bool GetConfigData(const std::string& section, const std::string& group, const std::string& key, double& value) ;
  bool GetConfigData(const std::string& section, const std::string& group, const std::string& key, std::string& value) ;
  bool GetConfigData(const std::string& section, const std::string& group, const std::string& key, std::vector<std::string>& value) ;

  void AddActiveThread(PThread* thread);
  std::vector<PThread*> ActiveThreads();

  long GetTimer(TimerUserData* callback, const std::string& name, long timeout,
                bool oneShot = false,
                bool aligned = false) ;

  void Run() ;

  static AppContext* GetInstance();

  void ShutDownNow() ;

private:
  static PMutex& GetMutex();
  void OnTimer(const boost::system::error_code& e, Timer* timer) ;

private:
  AppContext() ;
  ~AppContext() ;
  void OnTimeout() ;
private:
  static bool is_destructed;
  struct PrivateData ;
  std::auto_ptr<PrivateData> d_data ;
};


#if defined(qApp)
#undef qApp
#endif
#define qApp AppContext::GetInstance()

#endif // APPCONTEXT_H
