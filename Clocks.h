//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 1/16/2015 Clocks.h **//


#ifndef CLOCKS_H
#define CLOCKS_H

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

class ClockUtils {
public:
  enum { USEC = 1000000, USEC15 = 15 * USEC, USEC30 = 30 * USEC, USEC45 = 45 * USEC, USEC60 = 60 * USEC } ;

  //inline static long long RealTimeNow() { return boost::posix_time::microsec_clock::universal_time().time_of_day().total_microseconds() ; }
  inline static long long RealTimeNow()
  {
    timespec ts ;

#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    ts.tv_sec = mts.tv_sec;
    ts.tv_nsec = mts.tv_nsec;
#else
    clock_gettime(CLOCK_REALTIME, &ts);
#endif

    return (static_cast<long long>(1000000000UL)*(static_cast<long long>(ts.tv_sec)%86400) + static_cast<long long>(ts.tv_nsec))/1000 ;
  }

  inline static long long DbTimeToSimTime(long long tm) {
    return boost::posix_time::time_duration((int)((double)(tm % 1000000000000) / 10000000000),
                                     (int)((double)(tm % 10000000000) / 100000000),
                                     (int)((double)(tm % 100000000) / 1000000),
                                     (int)((double)(tm % 1000000))).total_microseconds() ;
  }

  inline static void UsecToTime(long long intime, int& hours, int& minutes, int& seconds, int& usecs) {
    boost::posix_time::time_duration td = boost::posix_time::microseconds(intime) ;
    hours = td.hours() ; minutes = td.minutes() ; seconds = td.seconds() ; usecs = td.fractional_seconds() ;
  }

  inline static std::string UsecToString(long long intime) {
    //return (boost::posix_time::to_simple_string(boost::posix_time::microseconds(intime)).c_str()) ;
    timespec ts ;
    ts.tv_sec = (intime/1000000) ;
    long msec = (intime % 1000000) ;
    struct tm t ;
    localtime_r(&(ts.tv_sec),&t) ;
    char buff[30] ;
    size_t l = strftime(buff,30,"%T",&t) ;
    sprintf(buff+l,".%ld", msec) ;
    return buff ;
  }

  inline static std::string UsecToUTCString(long long intime) {
    //return (boost::posix_time::to_simple_string(boost::posix_time::microseconds(intime)).c_str()) ;
    timespec ts ;
    ts.tv_sec = (intime/1000000) ;
    long msec = (intime % 1000000) ;
    struct tm t ;
    gmtime_r(&(ts.tv_sec),&t) ;
    char buff[30] ;
    size_t l = strftime(buff,30,"%T",&t) ;
    sprintf(buff+l,".%ld", msec) ;
    return buff ;
  }

  inline static long long TimeToUsec(int hours, int minutes, int seconds, int usecs) {
    return boost::posix_time::time_duration(hours, minutes, seconds, usecs).total_microseconds() ;
  }
};

#endif // CLOCKS_H
