//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 3/25/2015 TCPSocket.cpp **//

#include "TCPSocket.h"
#include "Logger.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <poll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <ctype.h>

TCPSocket::TCPSocket(const std::string& ipA, int portA, const std::string& ipB, int portB)
  : m_ipA(ipA), m_portA(portA), m_ipB(ipB), m_portB(portB),
    m_sd(-1), m_flip(false)
{
  m_sockIP = m_ipA ;
  m_sockPort = m_portA ;
}

TCPSocket::~TCPSocket()
{
  if (m_sd > 0) {
    ::close(m_sd);
  }
}

bool TCPSocket::Restart()
{
  if (m_sd > 0) {
    ::close(m_sd) ;
    m_sd = -1 ;
  }

  m_sd = socket(AF_INET, SOCK_STREAM, 0);

  if (m_sd < 0) {
    return false ;
  }

  memset(&m_serverAddr, 0x00, sizeof(struct sockaddr_in));

  m_serverAddr.sin_family = AF_INET;
  m_serverAddr.sin_addr.s_addr = inet_addr(m_sockIP.c_str());

  if (m_serverAddr.sin_addr.s_addr == INADDR_NONE) {
    struct hostent* n;
    if ((n = gethostbyname(m_sockIP.c_str())) == NULL) {
      LOG_ERROR1("gethostbyname: %s", strerror(errno)) ;
      return false ;
    }

    memcpy(&m_serverAddr.sin_addr, (char*)n->h_addr_list[0], n->h_length);
  }

  m_serverAddr.sin_port = htons(m_sockPort);

  int flag = 1;

  if (setsockopt(m_sd, IPPROTO_TCP , TCP_NODELAY, (char*)&flag, sizeof(flag)) < 0) {
    return false ;
  }

  int rc = connect(m_sd, (struct sockaddr*)&m_serverAddr, sizeof(m_serverAddr));

  if (rc < 0) {
    LOG_ERROR2("TCP Socket::Failed to connect - ip:%s, port:%d", m_sockIP.c_str(), m_sockPort);

    LOG_DEBUG5("flip:%s ipA:%s portA:%d ipB:%s portB:%d",
              m_flip ? "true" : "false",
              m_ipA.c_str(), m_portA, m_ipB.c_str(), m_portB) ;

    if ((m_ipB != "") && (m_portB != 0)) {
      m_flip = !m_flip ;
      if (m_flip) {
        m_sockIP   = m_ipA ;
        m_sockPort = m_portA ;
        LOG_DEBUG3("flip:%s sockIP:%s sockPort:%d",
                  m_flip ? "true" : "false",
                  m_sockIP.c_str(), m_sockPort) ;
      } else {
        m_sockIP   = m_ipB ;
        m_sockPort = m_portB ;
        LOG_DEBUG3("flip:%s sockIP:%s sockPort:%d",
                  m_flip ? "true" : "false",
                  m_sockIP.c_str(), m_sockPort) ;
      }
      LOG_DEBUG2("Switch to Backup ip:%s port:%d", m_sockIP.c_str(), m_sockPort) ;
    }

    return false;
  }

  return true;
}

int TCPSocket::GetSockID()
{
  return m_sd;
}

bool TCPSocket::Send(const char* buf, int nbytes)
{
  ssize_t offset = 0;
  ssize_t amt = nbytes;

  LOG_DEBUG1("Send:%d", nbytes) ;

  while (amt) {

      int sbytes = send(m_sd, buf + offset, amt, MSG_NOSIGNAL);

      // LOG_DEBUG1("send() rc=%d", (int)sbytes) ;

      if (sbytes < 0) {
          if (errno == EINTR || errno == EAGAIN) { continue; }
          LOG_ERROR1("send() error: %s", strerror(errno)) ;
          return false ;
        }

      offset += sbytes;
      amt -= sbytes;
  }

  return true ;
}

void TCPSocket::Close()
{
  ::close(m_sd);
  m_sd = -1 ;
}

const std::string& TCPSocket::sockIP() const
{
  return m_sockIP;
}

int TCPSocket::sockPort() const
{
  return m_sockPort;
}



