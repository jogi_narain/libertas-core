CONFIG  -= qt
TARGET = libertas-core
TEMPLATE = lib
CONFIG += staticlib

INCLUDEPATH += /usr/local/include
INCLUDEPATH += ../include

# Set binary name
CONFIG(debug, debug|release) {
  unix: TARGET = $$join(TARGET,,,_debug)
  else: TARGET = $$join(TARGET,,,d)
}

HEADERS += \
    ConfigUtils.h \
    Jzon.h \
    AppContext.h \
    Logger.h \
    PMutex.h \
    PPThread.h \
    Timer.h \
    Clocks.h \
    RingBuffer.h \
    TCPSocket.h

SOURCES += \
    Jzon.cpp \
    ConfigUtils.cpp \
    Logger.cpp \
    PPThread.cpp \
    Timer.cpp \
    AppContext.cpp \
    RingBuffer.cpp \
    TCPSocket.cpp

# Headers copied
headers.path = ../include
headers.files = $$HEADERS
INSTALLS += headers

# Libray copy
DESTDIR = ../libs

# Very dubios -- had to add it after rebuild of env. It tries to strip .h files
# in release mode. Then complains
# Need to fix this see http://www.gossamer-threads.com/lists/mythtv/dev/141885
QMAKE_STRIP = echo

# Optimise for speed
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3
#QMAKE_CXXFLAGS_RELEASE += -O0 -g

CONFIG( release, debug|release ) {
  DEFINES += KL_NO_DEBUG=1
  DEFINES += NDEBUG
  DEFINES += BOOST_DISABLE_ASSERTS
}
