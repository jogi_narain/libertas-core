//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 1/16/2015 ConfigUtils.cpp **//

#include "ConfigUtils.h"
#include "AppContext.h"

bool ConfigUtils::LoadTextFile(const std::string &fileName, Jzon::Object& data)
{
    Jzon::FileReader reader(fileName) ;

    if (!reader.Read(data)) {
      LOG_ERROR1("ERROR: %s", reader.GetError().c_str()) ;
      return false ;
    }

    return true ;
}

bool ConfigUtils::GetDataClientConfig(const std::string& /*configId*/, std::string& /*host*/, int& /*port*/,
                                      std::string& /*latencyFile*/)
{
//  if (!qApp->GetConfigData("FEEDHANDLERS", configId, "Host", host)) {
//    LOG_ERROR1("FAILED: to find [FEEDHANDLERS] %s/Host", qPrintable(configId)) ;
//    return false ;
//  }

//  if (!qApp->GetConfigData("FEEDHANDLERS", configId, "Port", port)) {
//    LOG_ERROR1("FAILED: to find [FEEDHANDLERS] %s/Port", qPrintable(configId)) ;
//    return false ;
//  }

//  if (!qApp->GetConfigData("LATENCYMAP", configId, "File", latencyFile)) {
//    LOG_ERROR1("FAILED: to find [LATENCYMAP] %s/File", qPrintable(configId)) ;
//    return false ;
//  }

  return true ;
}

