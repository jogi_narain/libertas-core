//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 3/30/2015 RingBuffer.h **//


#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include <iostream>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#include "PMutex.h"

class RingBuffer {
private:
           char*            m_data;
  volatile uint32_t         m_readIdx;
  volatile uint32_t         m_writeIdx;
  volatile uint32_t         m_endPosIdx;
           uint32_t         m_buflen ;

           uint32_t         count;
           pthread_cond_t   nonzero;
           PMutex           lock;

public:
  RingBuffer(int buffsize = (1024 * 1024));
  ~RingBuffer() {}

  bool Write(const char* data, int len);
  const char* Read(int& len);
  const char* ReadWait(int& len);
  const char* ReadTimedWait(int secs, int& len);

private:
  const char* NoLockRead(int& len);
  bool NoLockWrite(const char* data, int len);
};

#endif // RINGBUFFER_H
