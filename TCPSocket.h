//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 3/25/2015 TCPSocket.h **//


#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <netinet/tcp.h>

class TCPSocket {
public:
  TCPSocket(const std::string& ipA, int portA, const std::string& ipB = std::string(), int portB = 0) ;
  virtual ~TCPSocket();

  bool Init(std::string ip, int port) ;

  bool Restart();
  int GetSockID() ;

  bool Send(const char* buf, int nbytes) ;
  void Close() ;

  const std::string& sockIP() const;
  int sockPort() const;

private:
  std::string         m_ipA;
  int                 m_portA;

  std::string         m_ipB;
  int                 m_portB;

  std::string         m_sockIP;
  int                 m_sockPort;

  int                 m_sd;
  bool                m_isGood ;

  bool                m_flip ;

  struct sockaddr_in m_serverAddr;
};

#endif // TCPSOCKET_H
