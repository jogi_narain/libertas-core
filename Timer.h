//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 1/16/2015 Timer.h **//


#ifndef TIMER_H
#define TIMER_H

#include <string>
#include <inttypes.h>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

struct TimerUserData ;
class Timer : public boost::asio::deadline_timer
{
  static uint32_t m_globalId ;
public:

  Timer(boost::asio::io_service& io_service,
        long long timeout = 0,
        const std::string& name = std::string(),
        TimerUserData* userData = NULL,
        bool isOneShot = false,
        long long timeFrom = 0,
        bool aligned = false
        );

  inline long TimerId() const { return m_timerId; }
  inline long long TimeOut() const { return m_timeout; }
  inline const std::string& Name() const { return m_name; }
  inline long long TimeFrom() const { return m_timeFrom; }
  inline void TimeFrom(long long offset) { m_timeFrom = offset ; }

  inline bool OneShot() const { return m_isOneShot; }
  inline bool IsAligned() const { return m_aligned; }
  inline TimerUserData* UserData() const { return m_userData; }

private:
  // Not copyable
  Timer(const Timer& rhs);
  Timer& operator=(const Timer& rhs);

private:
  uint32_t m_timerId ;
  long long m_timeout ;
  std::string m_name ;
  long long m_timeFrom ;
  bool m_isOneShot ;
  TimerUserData* m_userData ;
  bool m_aligned ;
};

struct TimerUserData
{
  virtual void OnTimer(const Timer& timer) = 0 ;
  virtual ~TimerUserData() {}
};

#endif // TIMER_H
