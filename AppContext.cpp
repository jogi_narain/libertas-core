//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 1/16/2015 AppContext.cpp **//

#include "AppContext.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>


#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

//#include <boost/thread.hpp>
//#include <boost/thread/mutex.hpp>

#include <boost/algorithm/string.hpp>

#include <boost/asio.hpp>

#include <algorithm>
#include <map>
#include <boost/any.hpp>

bool AppContext::is_destructed = (AppContext::GetMutex(), false);

typedef std::map<std::string, boost::any> ConfigDataMap ;

struct AppContext::PrivateData {

  boost::asio::io_service                       io;
  std::auto_ptr<boost::asio::deadline_timer>    timer ;
  boost::asio::strand                           strand;
  //boost::mutex                                  m_timerMutex ;
  PMutex                                m_timerMutex ;

  std::string                           m_shutdownTime ;
  std::string                           m_start_dt ;
  std::string                           m_end_dt ;

  std::string                           m_environment ;
  std::string                           m_config_file ;
  std::vector<PThread*>                 m_activeThreads ;
  //boost::mutex                          m_mutex ;
  PMutex                                m_mutex ;

  ConfigDataMap                                 m_configDataMap ;
  boost::property_tree::ptree                   m_configData ;
  std::map<long, boost::shared_ptr<Timer> >     m_timerMap ;

  PrivateData() : strand(io) {}

  bool GetConfigData(const std::string& section, const std::string& group, const std::string& key, boost::any& value) ;
  bool GetSettingData(const std::string& section, const std::string& group, const std::string& key, boost::any& value) ;
};

PMutex& AppContext::GetMutex()
{
  //static boost::mutex mutex;
  static PMutex mutex ;
  return mutex;
}

bool AppContext::PrivateData::GetConfigData(const std::string& section, const std::string& group, const std::string& key, boost::any& value)
{
  std::string compKey = section + "." + group + "." + key ;
  bool ret_code = false ;

  ConfigDataMap::iterator it = m_configDataMap.find(compKey) ;

  if (it != m_configDataMap.end()) {
    value = it->second ;
    LOG_INFO2("CONFIG OVERRIDE: --%s=%s", compKey.c_str(), (boost::any_cast<std::string>(&value))->c_str()) ;
    ret_code = true ;
  }
  else {
    if ((m_config_file != "") && GetSettingData(section, group, key, value)) {
      ret_code = true ;
    }
  }

  return ret_code ;
}

bool AppContext::PrivateData::GetSettingData(const std::string& section, const std::string& group, const std::string& key, boost::any& value)
{
  bool ret_code = false ;

  std::string compKey = section + "." + group + "\\" + key ;
  std::string keyValue ;

  try {
    keyValue = m_configData.get<std::string>(compKey) ;
  }
  catch (...) {
    LOG_ERROR3("CONFIG: [%s] %s/%s NOT found!", section.c_str(), group.c_str(), key.c_str()) ;
  }

  if (keyValue != "") {
    std::vector<std::string> keyValueList ;
    boost::split(keyValueList, keyValue, boost::is_any_of(","));

    //if (keyValueList.size() > 1) {
    if (key == "List") {
      LOG_INFO4("CONFIG: [%s] %s/%s=%s (List)", section.c_str(), group.c_str(), key.c_str(), keyValue.c_str()) ;
      value = keyValueList ;
    }
    else {
      LOG_INFO4("CONFIG: [%s] %s/%s=%s", section.c_str(), group.c_str(), key.c_str(), keyValue.c_str()) ;
      value = keyValue ;
    }

    ret_code = true ;
  }

  return ret_code ;
}

char* getCmdOption(char** begin, char** end, const std::string& option)
{
  char** itr = std::find(begin, end, option);

  if (itr != end && ++itr != end) {
    return *itr;
  }

  return 0;
}

AppContext::AppContext() : d_data(new PrivateData())
{
}

void AppContext::ShutDownNow()
{
   LOG_ERROR("***** SHUTDOWN as result of USER REQUEST *****") ;

   //boost::mutex::scoped_lock lock(d_data->m_timerMutex);
   ScopedPLock lock(d_data->m_timerMutex);

#if 0
    boost::posix_time::time_duration timeout = boost::posix_time::milliseconds(10);
    for (size_t i = 0 ; i < d_data->m_activeThreads.size() ; i++) {
      boost::thread* t = d_data->m_activeThreads[i] ;
      LOG_INFO1("Thread->interrupt(): 0x%x", t) ;
      t->interrupt() ;
    }
#endif

    d_data->io.stop() ;
}

void AppContext::OnTimeout()
{
  //boost::mutex::scoped_lock lock(d_data->m_timerMutex);
  ScopedPLock lock(d_data->m_timerMutex);
  LOG_INFO1("Shutdown as result of timeout.Active Threads: %d", d_data->m_activeThreads.size()) ;

  d_data->io.stop() ;

  for (size_t i = 0 ; i < d_data->m_activeThreads.size() ; i++) {
    PThread* t = d_data->m_activeThreads[i] ;
    LOG_INFO1("Thread->Cancel(): %ld", t->GetThreadId()) ;
    t->Cancel();
  }

  for (size_t i = 0 ; i < d_data->m_activeThreads.size() ; i++) {
    PThread* t = d_data->m_activeThreads[i] ;
    LOG_INFO1("Thread->Kill(): %ld", t->GetThreadId()) ;
    int rc = t->Kill(SIGINT);
    LOG_INFO3("Thread: %ld SIGINT=(%d) %s", t->GetThreadId(), rc, strerror(rc)) ;
  }

#if 0
  boost::posix_time::time_duration timeout = boost::posix_time::milliseconds(10);
  for (size_t i = 0 ; i < d_data->m_activeThreads.size() ; i++) {
    boost::thread* t = d_data->m_activeThreads[i] ;
    LOG_INFO1("Thread->interrupt(): 0x%x", t) ;
    t->interrupt() ;
  }

  for (size_t i = 0 ; i < d_data->m_activeThreads.size() ; i++) {
    boost::thread* t = d_data->m_activeThreads[i] ;
    LOG_INFO1("Thread->timed_join(): 0x%x", t) ;
    if (t->timed_join(timeout)) {
      LOG_INFO1("Thread->timed_join(): 0x%x -- OK", t) ;
    }
    else {
      LOG_ERROR1("Thread->timed_join(): 0x%x -- FAILED", t) ;
    }
  }
#endif
  d_data->io.stop() ;
}

AppContext* AppContext::GetInstance()
{
  assert(!is_destructed);
  (void)is_destructed; // prevent removing is_destructed in Release configuration

  //boost::mutex::scoped_lock lock(GetMutex());
  ScopedPLock lock(GetMutex());
  static AppContext instance;
  return &instance;
}

bool AppContext::Init(int& argc, char** argv)
{
  d_data->m_environment = "DEV" ;

  char* filename = getCmdOption(argv, argv + argc, "-f");

  if (filename) {
    d_data->m_config_file = filename ;
    boost::property_tree::ini_parser::read_ini(d_data->m_config_file, d_data->m_configData);
    //std::string str = pt.get<std::string>("DEFAULT.Environment\\Type") ;
  }

  for (int i = 0 ; i < argc ; i++) {
    std::string prefix = "--";
    std::string argument = argv[i];

    if (argument.substr(0, prefix.size()) == prefix) {
      std::string compKey = argument.substr(prefix.size());
      std::vector<std::string> compKeyArgs ;
      boost::split(compKeyArgs, compKey, boost::is_any_of("="));

      if (compKeyArgs.size() == 2) {
        std::string keyComp = compKeyArgs.at(0) ;
        std::string keyValue = compKeyArgs.at(1) ;
        std::vector<std::string> keyList ;
        boost::split(keyList, keyComp, boost::is_any_of("."));

        if (keyList.size() == 3) {
          // We have a good key value. Check if we have a list
          std::vector<std::string> keyValueList ;
          boost::split(keyValueList, keyValue, boost::is_any_of(","));

          if (keyValueList.size() > 1) {
            d_data->m_configDataMap.insert(std::make_pair(keyComp, keyValueList)) ;
          }
          else {
            d_data->m_configDataMap.insert(std::make_pair(keyComp, keyValue)) ;
          }
        }
      }
    }
  }

  {
    // Switch on at least all the errors
    LoggerEngine::LOGGER_LEVEL logLevel = LoggerEngine::LEVEL_ERROR ;

    std::string logLevelStr ;

    if (GetConfigData("DEFAULT", "Logger", "LogLevel", logLevelStr)) {
      if (logLevelStr == "DEBUG") {
        logLevel = LoggerEngine::LEVEL_DEBUG ;
      }
      else if (logLevelStr == "INFO") {
        logLevel = LoggerEngine::LEVEL_INFO ;
      }
      else if (logLevelStr == "WARN") {
        logLevel = LoggerEngine::LEVEL_WARN ;
      }
      else if (logLevelStr == "ERROR") {
        logLevel = LoggerEngine::LEVEL_ERROR ;
      }
      else if (logLevelStr == "FATAL") {
        logLevel = LoggerEngine::LEVEL_FATAL ;
      }
      else if (logLevelStr == "CRITICAL") {
        logLevel = LoggerEngine::LEVEL_CRITICAL ;
      }
      else if (logLevelStr == "NOTIFY") {
        logLevel = LoggerEngine::LEVEL_NOTIFY ;
      }
    }

    std::string logFileDir ;
    if (!GetConfigData("DEFAULT", "LogFile", "Dir", logFileDir)) {
      logFileDir = "./" ;
    }

    std::string logFileName ;
    if (GetConfigData("DEFAULT", "LogFile", "Name", logFileName)) {
      LoggerEngine::FileAppender(logFileDir, logFileName, logLevel) ;
    }

    boost::any value ;
    if (d_data->GetConfigData("DEFAULT", "Environment", "Type", value)) {
      d_data->m_environment = *(boost::any_cast<std::string>(&value)) ;
    }
  }
  {
      std::string value ;
      if (this->GetConfigData("DEFAULT", "Shutdown", "Time", value)) {
        d_data->m_shutdownTime = value ;

        //boost::posix_time::ptime now(boost::posix_time::second_clock::universal_time()) ;
        boost::posix_time::ptime now(boost::posix_time::second_clock::local_time()) ;
        boost::posix_time::time_duration t = boost::posix_time::duration_from_string(d_data->m_shutdownTime) ;
        int timeout = (t.total_seconds() - now.time_of_day().total_seconds()) ;
        boost::posix_time::time_duration when = now.time_of_day() + boost::posix_time::seconds(timeout) ;

        if (timeout < 0) {
            timeout = 30 ;
            LOG_ERROR1("Shutdown Time in the past, SHUTDOWN in %d secs", timeout) ;
        }

        LOG_WARN2("Shutdown at: %s secs(%d)", boost::posix_time::to_simple_string(when).c_str(), timeout) ;
        d_data->timer.reset(new boost::asio::deadline_timer(d_data->io, boost::posix_time::seconds(timeout))) ;
        d_data->timer->async_wait(d_data->strand.wrap(boost::bind(&AppContext::OnTimeout, this/*, boost::asio::placeholders::error*/))) ;

      } else {
        LOG_ERROR("Shutdown instruction not found!") ;
        return false ;
      }
  }

  return true ;

}

void AppContext::Run()
{
  d_data->io.run() ;
}

void AppContext::OnTimer(const boost::system::error_code& e, Timer* timer)
{
    //boost::mutex::scoped_lock lock(d_data->m_timerMutex);
    ScopedPLock lock(d_data->m_timerMutex);
    if (e ==  boost::asio::error::operation_aborted) {
      LOG_ERROR1("Timer aborted: %s - %s", e.message().c_str()) ;
      return;
    }

    if (!timer->OneShot()) {
      timer->UserData()->OnTimer((*timer));
      if (timer->IsAligned()) {
          boost::posix_time::ptime currentTime(boost::posix_time::microsec_clock::universal_time()) ;
          boost::posix_time::ptime fireTime = currentTime + boost::posix_time::seconds(timer->TimeOut() / 1000) ;
          boost::posix_time::ptime refTime(fireTime.date(),
                                            boost::posix_time::hours(fireTime.time_of_day().hours()) +
                                            boost::posix_time::minutes(fireTime.time_of_day().minutes())
                                            ) ;
          boost::posix_time::time_duration t = (refTime - fireTime) ;
          timer->TimeFrom(t.total_milliseconds()) ;
      }
      LOG_DEBUG2("Re-Arming %s at %d millisecs", timer->Name().c_str(), (timer->TimeFrom() + timer->TimeOut())) ;
      timer->expires_from_now(boost::posix_time::microseconds((timer->TimeFrom() + timer->TimeOut()) * 1000)) ;
      timer->async_wait(d_data->strand.wrap(boost::bind(&AppContext::OnTimer, this, boost::asio::placeholders::error, timer))) ;
    } else {
      long id = timer->TimerId() ; // Get the id
      timer->UserData()->OnTimer((*timer)); // Fire the timer
      LOG_DEBUG1("Deleting Timer %s", timer->Name().c_str()) ;
      boost::shared_ptr<Timer> t = d_data->m_timerMap[id] ;
      t->cancel() ;
      d_data->m_timerMap.erase(id) ; // Remove the timer, deletes at end of this scope
    }
}

long AppContext::GetTimer(TimerUserData* callback, const std::string& name, long timeout, bool oneShot, bool aligned)
{
    //boost::mutex::scoped_lock lock(d_data->m_timerMutex);
    ScopedPLock lock(d_data->m_timerMutex);

    long timeFrom = 0 ;
    if (aligned) {
        boost::posix_time::ptime currentTime(boost::posix_time::microsec_clock::universal_time()) ;
        boost::posix_time::ptime fireTime = currentTime + boost::posix_time::seconds(timeout / 1000) ;

        boost::posix_time::ptime refTime(fireTime.date(),
                                          boost::posix_time::hours(fireTime.time_of_day().hours()) +
                                          boost::posix_time::minutes(fireTime.time_of_day().minutes())
                                          ) ;
        boost::posix_time::time_duration t = (refTime - fireTime) ;
        timeFrom = t.total_milliseconds() ;
    }


    boost::shared_ptr<Timer> timer(new Timer(d_data->io, timeout, name, callback, oneShot, timeFrom, aligned)) ;
    //timer->expires_from_now(boost::posix_time::microseconds(timer->TimeOut() * 1000)) ;
    timer->expires_from_now(boost::posix_time::microseconds((timer->TimeFrom() + timer->TimeOut()) * 1000)) ;
    timer->TimeFrom(0) ; // reset offset
    timer->async_wait(d_data->strand.wrap(boost::bind(&AppContext::OnTimer, this, boost::asio::placeholders::error, timer.get()))) ;
    long id = timer->TimerId() ;
//    LOG_INFO4("Creating new timer=%ld %s at from=%ld timeout=%ld millisecs", id, name.c_str(),
//              timeFrom, timeout) ;
    d_data->m_timerMap.insert(std::make_pair(id, timer)) ;
    return id ;
}

boost::asio::io_service& AppContext::IOService()
{
   return d_data->io ;
}

AppContext::~AppContext()
{
  is_destructed = true;
}

bool AppContext::GetConfigData(const std::string& section, const std::string& group, const std::string& key, bool& value)
{
  boost::any v ;

  if (d_data->GetConfigData(section, group, key, v)) {
    std::string strval = (*boost::any_cast<std::string>(&v)) ;
    value = boost::lexical_cast<bool>(strval) ;
    return true ;
  }

  return false ;
}

bool AppContext::GetConfigData(const std::string& section, const std::string& group, const std::string& key, int& value)
{
  boost::any v ;

  if (d_data->GetConfigData(section, group, key, v)) {
    std::string strval = (*boost::any_cast<std::string>(&v)) ;
    value = boost::lexical_cast<int>(strval) ;
    return true ;
  }

  return false ;
}

bool AppContext::GetConfigData(const std::string& section, const std::string& group, const std::string& key, long& value)
{
  boost::any v ;

  if (d_data->GetConfigData(section, group, key, v)) {
    std::string strval = (*boost::any_cast<std::string>(&v)) ;
    value = boost::lexical_cast<long>(strval) ;
    return true ;
  }

  return false ;
}

bool AppContext::GetConfigData(const std::string& section, const std::string& group, const std::string& key, long long& value)
{
  boost::any v ;

  if (d_data->GetConfigData(section, group, key, v)) {
    std::string strval = (*boost::any_cast<std::string>(&v)) ;
    value = boost::lexical_cast<long long>(strval) ;
    return true ;
  }

  return false ;
}

bool AppContext::GetConfigData(const std::string& section, const std::string& group, const std::string& key, double& value)
{
  boost::any v ;

  if (d_data->GetConfigData(section, group, key, v)) {
    std::string strval = (*boost::any_cast<std::string>(&v)) ;
    value = boost::lexical_cast<double>(strval) ;
    return true ;
  }

  return false ;
}

bool AppContext::GetConfigData(const std::string& section, const std::string& group, const std::string& key, std::string& value)
{
  boost::any v ;

  if (d_data->GetConfigData(section, group, key, v)) {
    value = (*boost::any_cast<std::string>(&v)) ;
    return true ;
  }

  return false ;
}

bool AppContext::GetConfigData(const std::string& section, const std::string& group, const std::string& key, std::vector<std::string>& value)
{
  boost::any v ;

  if (d_data->GetConfigData(section, group, key, v)) {
    value = (*boost::any_cast< std::vector<std::string> >(&v)) ;
    return true ;
  }

  return false ;
}

//QDateTime AppContext::StartDate()
//{
//  return d_data->m_start_dt ;
//}

//QDateTime AppContext::EndDate()
//{
//  return d_data->m_end_dt ;
//}

//QString AppContext::GetEnv()
//{
//  return d_data->m_environment ;
//}

void AppContext::AddActiveThread(PThread *thread)
{
  //boost::mutex::scoped_lock lock(d_data->m_mutex);
  ScopedPLock lock(d_data->m_mutex) ;
  d_data->m_activeThreads.push_back(thread) ;
}

std::vector<PThread*> AppContext::ActiveThreads()
{
  //boost::mutex::scoped_lock lock(d_data->m_mutex);
  ScopedPLock lock(d_data->m_mutex) ;
  return d_data->m_activeThreads ;
}

//void AppContext::OnShutdown()
//{
//  // Set up a 10sec timeout, in case mail server is broken
//  QTimer::singleShot(10000, this, SLOT(OnTimeoutShutdown()));

//  //  // Send last mail to say that shutdown occurred
//  //  connect(d_data->m_smtpAppender, SIGNAL(Status(const int, const QString&)), this, SLOT(OnLastSmtpMsg(const int, const QString&))) ;
//  LOG_NOTIFY("SHUTDOWN event received") ;
//}

//void AppContext::OnTimeoutShutdown()
//{
//  LOG_INFO1("Shutdown as result of timeout. Active Threads: %d", d_data->m_activeThreads.size()) ;

//  for (size_t i = 0 ; i < d_data->m_activeThreads.size() ; i++) {
//    PThread* t = d_data->m_activeThreads[i] ;
//    LOG_INFO1("Thread->Cancel(): %ld", t->GetThreadId()) ;
//    t->Cancel();
//  }

//  for (size_t i = 0 ; i < d_data->m_activeThreads.size() ; i++) {
//    PThread* t = d_data->m_activeThreads[i] ;
//    LOG_INFO1("Thread->Kill(): %ld", t->GetThreadId()) ;
//    int rc = t->Kill(SIGINT);
//    LOG_INFO3("Thread: %ld SIGINT=(%d) %s", t->GetThreadId(), rc, strerror(rc)) ;
//  }
//}

//void AppContext::ShutDown()
//{
//  LOG_WARN("Manual SHUTDOWN received") ;
//  OnShutdown() ;
//}

//void AppContext::OnLastSmtpMsg(const int evt, const QString& str)
//{
//  LOG_INFO1("%s", qPrintable(str)) ;

//  if (evt == Smtp::RcptSent) {
//    LOG_INFO1("Active Threads: %d", d_data->m_activeThreads.size()) ;

//    foreach(QThread * t, d_data->m_activeThreads) {
//      LOG_INFO2("Thread->quit(): 0x%lx %s", t, qPrintable(t->objectName())) ;
//      t->quit() ;
//    }

//    foreach(QThread * t, d_data->m_activeThreads) {
//      LOG_INFO2("Thread->wait(): 0x%lx %s", t, qPrintable(t->objectName())) ;
//      t->wait(1000) ;
//    }

//    emit ForceShutdown() ;
//  }
//}

