//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 3/30/2015 RingBuffer.cpp **//

#include "RingBuffer.h"

#include "Logger.h"

RingBuffer::RingBuffer(int buffsize): m_readIdx(0), m_writeIdx(0), m_endPosIdx(0)
{
  m_data = new char[buffsize];
  m_buflen = buffsize;
}

static void semaphore_notify(uint32_t& count, pthread_cond_t& nonzero)
{
  if (count == 0) {
    pthread_cond_signal(&nonzero);
  }

  count++;
}

static void semaphore_wait(uint32_t& count, pthread_cond_t& nonzero, pthread_mutex_t& lock)
{
  while (count == 0) {
    pthread_cond_wait(&nonzero, &lock);
  }

  count--;
}

static void semaphore_timedwait(int secs, uint32_t& count, pthread_cond_t& nonzero, pthread_mutex_t& lock)
{
  if (count == 0) {
    timespec ts ;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += secs ; // seconds into future
    pthread_cond_timedwait(&nonzero, &lock, &ts);
  } else {
    count--;
  }
}

static void realloc_buffer(char*& data_buff, uint32_t& buff_len)
{
  char* temp = new char[buff_len * 2];
  memcpy(temp, data_buff, buff_len) ;

  delete[] data_buff;
  buff_len *= 2;
  data_buff = temp;
}

bool RingBuffer::Write(const char* data, int len)
{
  ScopedPLock m_locker(lock) ;
  if (NoLockWrite(data, len)) {
    semaphore_notify(count, nonzero) ;
    return true ;
  }

  return false ;
}

bool RingBuffer::NoLockWrite(const char* data, int len)
{
  uint32_t r = m_readIdx;
  uint32_t w = m_writeIdx;
  // uint32_t e = m_endPosIdx;

  LOG_DEBUG4("shmbuf_write: readIdx=%d writeIdx=%d len=%d buflen=%d", r, w, len, m_buflen);

  if (r <= w) {
    if (w + len <= m_buflen) {
      memcpy(m_data + w, data, len);
      m_writeIdx += len;
      return true;
    }

    // otherwise w+len > space ... no enough space to write
    if ((uint32_t)len >= r) {
      LOG_INFO2("not enough space to write len:%d >= r:%d", len, (int)r);
      realloc_buffer(m_data, m_buflen) ;
    }

    // from here, ok we have enough space to write; change the m_endPosIdx and m_writeIdx;
    m_endPosIdx = w;
    memcpy(m_data, data, len);
    m_writeIdx = len;
    return true;
  }
  else {
    LOG_DEBUG4("end of buffer: readIdx=%d writeIdx=%d endPosIdx=%d len=%d", m_readIdx, m_writeIdx, m_endPosIdx, len);
    // no space to write
    if (w + len >= r) {
      LOG_INFO3("no space left to write - w:%d + len:%d >= r:%d", (int)w, (int)len, (int)r) ;
      realloc_buffer(m_data, m_buflen) ;
    }

    // we have enough space
    memcpy(m_data + w, data, len);
    m_writeIdx += len;
    return true;
  }

  return true ;
}

const char* RingBuffer::ReadWait(int& len)
{
  ScopedPLock m_locker(lock) ;
  semaphore_wait(count, nonzero, lock.Mutex()) ;
  return NoLockRead(len) ;
}

const char* RingBuffer::ReadTimedWait(int secs, int& len)
{
  ScopedPLock m_locker(lock) ;
  semaphore_timedwait(secs, count, nonzero, lock.Mutex()) ;
  return NoLockRead(len) ;
}

const char* RingBuffer::Read(int& len)
{
  ScopedPLock m_locker(lock) ;
  return NoLockRead(len) ;
}

const char* RingBuffer::NoLockRead(int& len)
{
  uint32_t r = m_readIdx;
  uint32_t w = m_writeIdx;
  uint32_t e = m_endPosIdx;

  len = 0;

  // no data in the buffer;
  if (r == w) {
    return NULL;
  }

  LOG_DEBUG4("shmbuf_read: readIdx=%d writeIdx=%d endPosIdx=%d buflen=%d", m_readIdx, m_writeIdx, m_endPosIdx, m_buflen);

  if (r < w) {
    len = w - r;
    if (len > 0) {
      LOG_DEBUG5("r < w: readIdx=%d writeIdx=%d endPosIdx=%d len=%d buflen=%d", m_readIdx, m_writeIdx, m_endPosIdx, (int)len, m_buflen);
    }
    return m_data + r ;
  }

  // here, r>w
  // uint32_t e = smb->endPosIdx;
  len = e - r;
  if (len <= 0) {
    LOG_DEBUG2("shmbuf_read r:%d > w:%d", (int)r, (int)w) ;
    LOG_DEBUG5("len <= 0: readIdx=%d writeIdx=%d endPosIdx=%d len=%d buflen=%d", m_readIdx, m_writeIdx, m_endPosIdx, (int)len, m_buflen);

    m_readIdx = 0;
    len = 0 ;
    return NULL ;
  }
  else {
    LOG_DEBUG5("else len <= 0: readIdx=%d writeIdx=%d endPosIdx=%d len=%d buflen=%d", m_readIdx, m_writeIdx, m_endPosIdx, (int)len, m_buflen);
    return (m_data + r);
  }

  LOG_DEBUG5("return data: readIdx=%d writeIdx=%d endPosIdx=%d len=%d buflen=%d", m_readIdx, m_writeIdx, m_endPosIdx, (int)len, m_buflen);

  return m_data ;
}
