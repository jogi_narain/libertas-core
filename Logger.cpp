//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 1/16/2015 Logger.cpp **//

#include "Logger.h"

#include <time.h>
#include <sys/time.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

char LogLevelArray[] = { 'D', 'I', 'W', 'E', 'C', 'F', 'X' } ;

FILE* LoggerEngine::m_fileOutput = stdout;
bool LoggerEngine::m_fileOwned = false;
LoggerEngine::LOGGER_LEVEL LoggerEngine::m_loggerLevel = LoggerEngine::LEVEL_INFO ;
LogNotifier* LoggerEngine::m_notifier = NULL ;

static const int BUFFER_SIZE = 8192 ;

void kl_logger(const char* file, int line, const char* func, int level, const char* fmt, ...)
{
  char buf[BUFFER_SIZE];
  buf[BUFFER_SIZE - 1] = '\0';
  va_list ap;
  va_start(ap, fmt);  // use variable arg list

  if (fmt) {
    vsnprintf(buf, BUFFER_SIZE - 1, fmt, ap);
  }

  va_end(ap);

  const char* msg = &buf[0] ;

  LoggerEngine::LogEvent(file, line, func, (LoggerEngine::LOGGER_LEVEL)level, msg) ;
}

LoggerEngine::LoggerEngine()
{
}

LoggerEngine::~LoggerEngine()
{
  if (m_fileOwned) {
    fflush(m_fileOutput) ;
    fclose(m_fileOutput) ;
  }
}

void LoggerEngine::FileAppender(const std::string& fileDir, const std::string& fileName,
                                int level, bool truncate, bool backup)
{
  m_loggerLevel = (LOGGER_LEVEL)level ;

  std::string fullFileName = fileDir + "/" + fileName ;

  if (backup) {
    timespec ts ;

#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    ts.tv_sec = mts.tv_sec;
    ts.tv_nsec = mts.tv_nsec;
#else
    clock_gettime(CLOCK_REALTIME, &ts);
#endif

    struct tm t ;
    localtime_r(&(ts.tv_sec),&t) ;
    char buff[256] ;
    strftime(buff,120,"%Y%m%dT%H%M%S",&t) ;
    std::string bkFileName = fileDir + "/" + buff + "." + fileName ;
    rename(fullFileName.c_str(), bkFileName.c_str()) ;
  }

  std::string mode = (truncate ? "w" : "a") ;
  m_fileOutput = fopen(fullFileName.c_str(), mode.c_str());

  if (m_fileOutput != NULL) {
    m_fileOwned = true ;
  }
  else {
    m_fileOutput = stderr ;
    LOG_ERROR1("Failed to open file: %s", fullFileName.c_str()) ;
  }
}

PMutex& LoggerEngine::GetMutex()
{
  static PMutex  m ;
  return m;
}

struct timespec ts;



void LoggerEngine::LogEvent(const char* file, int line, const char* func, int level, const char* msg)
{
  if (level >= m_loggerLevel) {
    {
      //boost::mutex::scoped_lock lock(GetMutex());
      ScopedPLock lock(GetMutex());
      timespec ts ;

#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    ts.tv_sec = mts.tv_sec;
    ts.tv_nsec = mts.tv_nsec;
#else
    clock_gettime(CLOCK_REALTIME, &ts);
#endif

      struct tm t ;
      localtime_r(&(ts.tv_sec),&t) ;
      char buff[4096] ;
      size_t l = strftime(buff,120,"%F %T",&t) ;
      int len = sprintf(buff+l,".%06ld <%c> [%lu] {%s:%d::%s} - %s",
                        ts.tv_nsec/1000, LogLevelArray[level], (long)pthread_self(), file, line, func, msg) ;

      fprintf(m_fileOutput, "%s\n", buff);
      fflush(m_fileOutput);

      if ((level == LEVEL_NOTIFY) && (m_notifier != NULL)) {
        m_notifier->OnLog(level, buff, len);
      }

    }
  }
}

