//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 1/16/2015 Timer.cpp **//

#include "Timer.h"

uint32_t Timer::m_globalId = 0 ;

Timer::Timer(boost::asio::io_service &io_service, long long timeout,
             const std::string &name, TimerUserData *userData, bool isOneShot, long long timeFrom, bool aligned)
: boost::asio::deadline_timer(io_service), m_timerId(++m_globalId), m_timeout(timeout),
  m_name(name), m_timeFrom(timeFrom), m_isOneShot(isOneShot), m_userData(userData), m_aligned(aligned)
{
}

