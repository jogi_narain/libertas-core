//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 1/16/2015 Logger.h **//


#ifndef LOGGER_H
#define LOGGER_H

#include <sstream>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#include "PMutex.h"

struct LogNotifier
{
  virtual void OnLog(int logLevel, const char* msg, int len) = 0 ;
};

class LoggerEngine {

public:
  typedef enum {
    LEVEL_DEBUG,
    LEVEL_INFO,
    LEVEL_WARN,
    LEVEL_ERROR,
    LEVEL_CRITICAL,
    LEVEL_FATAL,
    LEVEL_NOTIFY
  } LOGGER_LEVEL;


public:
  LoggerEngine() ;
  ~LoggerEngine();

public:
  static void FileAppender(const std::string& fileDir,
                           const std::string& fileName,
                           int level = LEVEL_INFO,
                           bool truncate = true, bool backup = true);

  static void LogEvent(const char* file, int line, const char* func, int level, const char* msg);

  static void SetLogNotifier(LogNotifier* notifier)
  {
    m_notifier = notifier ;
  }

private:
  static PMutex& GetMutex() ;
private:
  static FILE* m_fileOutput ;
  static bool  m_fileOwned ;
  static LOGGER_LEVEL m_loggerLevel ;
  static LogNotifier* m_notifier ;
};

extern "C" {
  void kl_logger(const char* file, int line, const char* func, int level, const char* fmt, ...);
}

#define _KL_FILE_ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define LOG_DEBUG(msg) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_DEBUG, "%s", msg) ; } while (false)

#define LOG_DEBUG1(fmt, ARG1) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_DEBUG, fmt, ARG1) ; } while (false)

#define LOG_DEBUG2(fmt, ARG1, ARG2) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_DEBUG, fmt, ARG1, ARG2) ; } while (false)

#define LOG_DEBUG3(fmt, ARG1, ARG2, ARG3) do { \
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_DEBUG, fmt, ARG1, ARG2, ARG3) ; }while (false)

#define LOG_DEBUG4(fmt, ARG1, ARG2, ARG3, ARG4) do { \
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_DEBUG, fmt, ARG1, ARG2, ARG3, ARG4) ; }while (false)

#define LOG_DEBUG5(fmt, ARG1, ARG2, ARG3, ARG4, ARG5) do { \
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_DEBUG, fmt, ARG1, ARG2, ARG3, ARG4, ARG5) ; }while (false)

#define LOG_DEBUG6(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6) do { \
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_DEBUG, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6) ; }while (false)

#define LOG_DEBUG7(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7) do { \
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_DEBUG, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7) ; }while (false)

#define LOG_DEBUG8(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8) do { \
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_DEBUG, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8) ; }while (false)


#define LOG_INFO(msg) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_INFO, "%s", msg) ; } while (false)

#define LOG_INFO1(fmt, ARG1) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_INFO, fmt, ARG1) ; } while (false)

#define LOG_INFO2(fmt, ARG1, ARG2) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_INFO, fmt, ARG1, ARG2) ; } while (false)

#define LOG_INFO3(fmt, ARG1, ARG2, ARG3) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_INFO, fmt, ARG1, ARG2, ARG3) ; }while (false)

#define LOG_INFO4(fmt, ARG1, ARG2, ARG3, ARG4) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_INFO, fmt, ARG1, ARG2, ARG3, ARG4) ; }while (false)

#define LOG_INFO5(fmt, ARG1, ARG2, ARG3, ARG4, ARG5) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_INFO, fmt, ARG1, ARG2, ARG3, ARG4, ARG5) ; }while (false)

#define LOG_INFO6(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_INFO, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6) ; }while (false)

#define LOG_INFO7(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_INFO, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7) ; }while (false)

#define LOG_INFO8(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_INFO, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8) ; }while (false)

#define LOG_INFO9(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8, ARG9) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_INFO, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8, ARG9) ; }while (false)


#define LOG_WARN(msg) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_WARN, "%s", msg) ; } while (false)

#define LOG_WARN1(fmt, ARG1) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_WARN, fmt, ARG1) ; } while (false)

#define LOG_WARN2(fmt, ARG1, ARG2) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_WARN, fmt, ARG1, ARG2) ; } while (false)

#define LOG_WARN3(fmt, ARG1, ARG2, ARG3) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_WARN, fmt, ARG1, ARG2, ARG3) ; }while (false)

#define LOG_WARN10(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8, ARG9, ARG10) do{\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_WARN, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8, ARG9, ARG10) ; } while (false)

#define LOG_ERROR(msg) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_ERROR, "%s", msg) ; } while (false)

#define LOG_ERROR1(fmt, ARG1) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_ERROR, fmt, ARG1) ; } while (false)

#define LOG_ERROR2(fmt, ARG1, ARG2) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_ERROR, fmt, ARG1, ARG2) ; } while (false)

#define LOG_ERROR3(fmt, ARG1, ARG2, ARG3) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_ERROR, fmt, ARG1, ARG2, ARG3) ; } while (false)

#define LOG_ERROR4(fmt, ARG1, ARG2, ARG3, ARG4) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_ERROR, fmt, ARG1, ARG2, ARG3, ARG4) ; } while (false)

#define LOG_ERROR5(fmt, ARG1, ARG2, ARG3, ARG4, ARG5) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_ERROR, fmt, ARG1, ARG2, ARG3, ARG4, ARG5) ; } while (false)

#define LOG_ERROR6(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_ERROR, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6) ; } while (false)

#define LOG_ERROR7(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_ERROR, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7) ; } while (false)

#define LOG_ERROR8(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_ERROR, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8) ; } while (false)

#define LOG_FATAL(msg) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_FATAL, "%s", msg) ; } while (false)

#define LOG_FATAL1(fmt, ARG1) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_FATAL, fmt, ARG1) ; } while (false)

#define LOG_FATAL2(fmt, ARG1, ARG2) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_FATAL, fmt, ARG1, ARG2) ; } while (false)

#define LOG_FATAL3(fmt, ARG1, ARG2, ARG3) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_FATAL, fmt, ARG1, ARG2, ARG3) ; } while (false)

#define LOG_CRITICAL(msg) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_CRITICAL, "%s", msg) ; } while (false)

#define LOG_CRITICAL1(fmt, ARG1) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_CRITICAL, fmt, ARG1) ; } while (false)

#define LOG_CRITICAL2(fmt, ARG1, ARG2) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_CRITICAL, fmt, ARG1, ARG2) ; } while (false)

#define LOG_CRITICAL3(fmt, ARG1, ARG2, ARG3) do {\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_CRITICAL, fmt, ARG1, ARG2, ARG3) ; } while (false)

#define LOG_NOTIFY(msg) do{\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_NOTIFY, "%s", msg) ; } while (false)

#define LOG_NOTIFY1(fmt, ARG1) do{\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_NOTIFY, fmt, ARG1) ; } while (false)

#define LOG_NOTIFY2(fmt, ARG1, ARG2) do{\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_NOTIFY, fmt, ARG1, ARG2) ; } while (false)

#define LOG_NOTIFY3(fmt, ARG1, ARG2, ARG3) do{\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_NOTIFY, fmt, ARG1, ARG2, ARG3) ; } while (false)

#define LOG_NOTIFY10(fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8, ARG9, ARG10) do{\
    kl_logger(_KL_FILE_, __LINE__, __FUNCTION__, (int)LoggerEngine::LEVEL_NOTIFY, fmt, ARG1, ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8, ARG9, ARG10) ; } while (false)

#if defined(KL_NO_DEBUG)
#undef LOG_DEBUG
#undef LOG_DEBUG1
#undef LOG_DEBUG2
#undef LOG_DEBUG3
#undef LOG_DEBUG4
#undef LOG_DEBUG5
#undef LOG_DEBUG6
#undef LOG_DEBUG7
#undef LOG_DEBUG8
#define LOG_DEBUG(msg) do{}while(false)
#define LOG_DEBUG1(fmt,ARG1) do{}while(false)
#define LOG_DEBUG2(fmt,ARG1,ARG2) do{}while(false)
#define LOG_DEBUG3(fmt,ARG1,ARG2, ARG3) do{}while(false)
#define LOG_DEBUG4(fmt,ARG1,ARG2, ARG3, ARG4) do{}while(false)
#define LOG_DEBUG5(fmt,ARG1,ARG2, ARG3, ARG4, ARG5) do{}while(false)
#define LOG_DEBUG6(fmt,ARG1,ARG2, ARG3, ARG4, ARG5, ARG6) do{}while(false)
#define LOG_DEBUG7(fmt,ARG1,ARG2, ARG3, ARG4, ARG5, ARG6, ARG7) do{}while(false)
#define LOG_DEBUG8(fmt,ARG1,ARG2, ARG3, ARG4, ARG5, ARG6, ARG7, ARG8) do{}while(false)
#endif

#define QPRINTABLE(msg)     msg.c_str()
#define QUNUSED(expr)       do { (void)(expr); } while (0)


#endif // LOGGER_H
