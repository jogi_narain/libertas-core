//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 1/16/2015 PMutex.h **//


#ifndef PMUTEX_H
#define PMUTEX_H

#include <pthread.h>

/// Simple object wrapper around pthread_mutex_t
struct PMutex {
  pthread_mutex_t mutex;
  PMutex() { pthread_mutex_init(&mutex, NULL); }
  pthread_mutex_t& Mutex() { return mutex ; }
  ~PMutex() { pthread_mutex_destroy(&mutex); }
};

/// Simple object wrapper around pthread_mutex_t to create scoped lock
struct ScopedPLock {
  PMutex& mutex;
  ScopedPLock(PMutex& m): mutex(m) { pthread_mutex_lock(&mutex.mutex); }
  ~ScopedPLock() { pthread_mutex_unlock(&mutex.mutex); }
};

#ifdef __MACH__ // OS X does not have spin locks
typedef int pthread_spinlock_t;

int pthread_spin_init(pthread_spinlock_t *lock, int /*pshared*/) ;
int pthread_spin_destroy(pthread_spinlock_t * /*lock*/) ;
int pthread_spin_lock(pthread_spinlock_t *lock) ;
int pthread_spin_trylock(pthread_spinlock_t *lock) ;
int pthread_spin_unlock(pthread_spinlock_t *lock) ;
#endif // __MACH__

/// Simple object wrapper around pthread_spinlock_t
struct PSpinLock {
  pthread_spinlock_t spinLock;
  PSpinLock() { pthread_spin_init(&spinLock, PTHREAD_PROCESS_PRIVATE) ;; }
  ~PSpinLock() { pthread_spin_destroy(&spinLock); }
};

/// Simple object wrapper around pthread_spinlock_t to create scoped lock
struct ScopedPSpinLock {
  PSpinLock& spinLock;
  ScopedPSpinLock(PSpinLock& m): spinLock(m) { pthread_spin_lock(&spinLock.spinLock); }
  ~ScopedPSpinLock() { pthread_spin_unlock(&spinLock.spinLock); }
};

#endif // PMUTEX_H
