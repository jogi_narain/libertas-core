//******************************************************************************************//
//**                                                                                      **//
//**      _   _                           ____                         _     _     ____   **//
//**     / \ | |_ _ __ ___ _   _ _   _   / ___|_ __ ___  _   _ _ __   | |   | |   / ___|  **//
//**    / _ \| __| '__/ _ \ | | | | | | | |  _| '__/ _ \| | | | '_ \  | |   | |  | |      **//
//**   / ___ \ |_| | |  __/ |_| | |_| | | |_| | | | (_) | |_| | |_) | | |___| |__| |___   **//
//**  /_/   \_\__|_|  \___|\__, |\__,_|  \____|_|  \___/ \__,_| .__/  |_____|_____\____|  **//
//**                       |___/                              |_|                         **//
//**  All code copyright (c) 2010-2015  FGS Capital LLC,viciTek LLC, Atreyu Group LLC     **//
//**  All rights reserved -- Jogi Narain                                                  **//
//**                                                                                      **//
//******************************************************************************************//

//** Created By: jnarain 1/16/2015 PPThread.h **//


#ifndef PPTHREAD_H
#define PPTHREAD_H

#include <pthread.h>
#include <signal.h>

/// Simple wrapper around pthread_t
class PThread {
public:
  PThread()  : m_threadId(0)
  {
  }

  virtual ~PThread()
  {
    if (m_threadId != 0) {
      (void)pthread_join(m_threadId, NULL);
    }
  }

  // Returns true if the thread was successfully started,
  //         false if there was an error starting the thread
  int Start()
  {
    return pthread_create(&m_threadId, NULL, InternalThreadEntryFunc, this) ;
  }

  void Join()
  {
    (void)pthread_join(m_threadId, NULL);
  }

  void Cancel()
  {
    if (m_threadId > 0) {
      pthread_cancel(m_threadId) ;
    }
  }

  int Kill(int sig)
  {
    return pthread_kill(m_threadId, sig) ;
  }

  pthread_t GetThreadId() const
  {
    return m_threadId ;
  }

  static pthread_t GetId()
  {
    return pthread_self();
  }

protected:
  virtual void Run() = 0;

private:
  static void* InternalThreadEntryFunc(void* This) {
    ((PThread*)This)->Run();
    return NULL;
  }
private:
  PThread(const PThread&);
  const PThread& operator=(const PThread&);

private:
  pthread_t m_threadId;
};

#endif // PPTHREAD_H
